<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStickersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stickers', function (Blueprint $table) {
            //				  /----- order-id
            //id, serial, batch, status, timestamps

            $table->increments('id');
            $table->string('serial');
            $table->integer('batch_id');
            $table->string('status', 2);
            $table->timestamps();

        });

        Schema::create('batches', function (Blueprint $table) {
            //				  /----- customer-id
            //id -- serial ---order-id, status - timestamps

            $table->increments('id');
            $table->string('serial');
            $table->integer('order_id');
            $table->string('status', 2);
            $table->date('expiry');
            $table->string('location');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stickers');
        Schema::drop('batches');
    }

}
