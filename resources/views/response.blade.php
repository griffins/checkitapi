<?php

function keys_to_lower($obj){

    if(is_array($obj)){
        $new_object =[];
        foreach ($obj as $key => $value) {
            if(is_array($value) || is_object($value)){
                $new_object[strtolower($key)] = keys_to_lower($value);
            }else{
                $new_object[strtolower($key)] = $value;
            }
        }

    }else if(is_object($obj)){
        $new_object = new stdClass();
        $attr = get_object_vars($obj);
        foreach ($attr as $key => $value) {
            if(is_array($value) || is_object($value)){
                $new_object->{strtolower($key)} = keys_to_lower($value);
            }else{
                $new_object->{strtolower($key)} = $value;
            }
        }
    }
    return $new_object;
}
?>

{
   "data": {!! json_encode(keys_to_lower(json_decode($payload)), JSON_PRETTY_PRINT)!!},
   "code": {{{$code}}}
}