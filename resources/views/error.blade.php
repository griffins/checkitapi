{
   "error": {
      "message": "{{ $message }}",
      "type": "{{ $type }}",
      "details" : "{{ $details or 'Not available'}}",
      "code": {{ $code }}
   }
}