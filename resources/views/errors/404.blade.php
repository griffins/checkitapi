{
   "error": {
      "message": "Unsupported request. Please read the API documentation",
      "type": "APIException",
      "code": 400
   }
}
