var logLevel = "debug";
var $customer;
var $ajaxContainer;
var $errorBag;
var $token;
var $action = "login";
var domChanged = true;
$(document).ready(function () {

    $ajaxContainer = $("#page_container");
    console.log("Starting app!");
    $token = getLocal("token", null);
    setupAjax();

    console.log("Gettting user data from server using: " + $token);

    $(document).bind("DOMNodeInserted", function (event) {
        console.log("DOMNodeInserted")
        if (domChanged) {
            $('form').on('submit', function (e) {
                e.preventDefault();
                $errorBag = $('.error_bag');
                $errorBag.addClass('hidden');
                submitForm($(this), function (response) {
                    switch ($action) {
                        case "login":
                            if (response.data.token != null) {
                                $token = response.data.token
                                saveLocal('token', $token);
                                setupAjax();
                                loadMenu();
                            }
                            break;
                        case "recieve":
                            showMessage('Batch ' + response.data.serial + ' has been marked recieved');
                            $('form').trigger('reset');
                            break;
                        case "dispatch":
                            showMessage('Batch ' + response.data.serial + ' has been marked dispatched');
                            $('form').trigger('reset');
                            break;
                        case "deactivate":
                            showMessage('Batch ' + response.data.serial + ' has been deactivated');
                            $('form').trigger('reset');
                            break;
                        case "enquire":
                            //showMessage('Batch ' + response.data.serial + ' has been deactivated');
                            $('#content_b').html('');
                            $('#content_b').removeClass('hidden');
                            $('form').trigger('reset');
                            jPut.batch.data = response.data;
                            break;
                        default:
                    }

                })
            });
            $('input').on('input', function (e) {
                console.log('inpt changed');
            })
            $('.ajax').on('click', function (e) {
                console.log("Loading page");
                e.preventDefault();
                loadPage($(this));
            })
            domChanged = false;
        }

    });

    makeCall("user", "GET", null, function ($response) {
        $customer = $response;
        loadMenu();
    }, function ($response) {
        loadLogin();
    });


});
function submitForm(form, callback) {
    $url = form.attr('action');
    $method = form.attr('method');
    $data = form.serialize();
    if ($errorBag != null) {
        $errorBag.html('');
    }
    makeCall($url, $method, $data, function (response) {
        callback(response);

    }, function (request) {
        response = JSON.parse(request.responseText);
        $errorBag.html(response.error.message);
        $errorBag.removeClass('hidden')
        console.log(response.error.message);
        $('form').trigger('reset');
    });
}
function loadPage(link) {

    $action = link.attr('data-action');
    loadLink($action + ".html");
    console.log("loading " + $action + " page:");
}
function loadLink(link) {
    domChanged = true;
    $method = 'get';

    makeCall(link, "GET", null, function ($response) {
        console.log("loading " + $action + " page:");
        $ajaxContainer.html($response);
        if ($action == "logout") {
            loadLogin();
        }
    }, null);

}
function loadMenu() {
    loadLink("menu.html");
}

function makeCall($url, $method, $data, $success, $failure) {
    console.log('making http request to: ' + $url)
    $.ajax({
        url: $url,
        method: $method,
        data: $data,
        error: function (request, b, c) {
            if (logLevel == "debug") {
                console.log("An error occurred when making request");
                //console.log(request.responseText);
            }
            if ($failure != null) {
                $failure(request);
            }
        },
        success: function (response, status) {
            if (logLevel == "debug") {
                console.log("Request succeded");
                //console.log(response.toString());
            }
            if ($success != null) {
                $success(response);
            }
        }
    });
}
function saveLocal($key, $value) {
    if (typeof(Storage) !== "undefined") {
        var value = localStorage.setItem($key, $value)
    }
}
function getLocal($key, $default) {
    if (typeof(Storage) !== "undefined") {
        var value = localStorage.getItem($key);
        if (value == null) {
            return $default;
        }
        return value;

    } else {
        return $default;
    }
}
function loadLogin() {
    $action = "login";
    loadLink("login.html");
}

function setupAjax() {
    console.log("adding token as header" + $token);
    $.ajaxSetup({
        headers: {'authorization': 'Bearer {' + $token + '}'}
    });

}

function showMessage(message) {
    setTimeout(function () {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success(message, 'CheckIT')
    });
}

function decodeLocation(latlng, callback) {
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDpK_KMaqW6P92tgoP3F4YPfEkC-lOdzy0&latlng=';
    makeCall($url + latlng, null, function (response) {
        place = response.results[0].address_components[0].long_name;
        callback(place)
    }, null, function (response) {
        callback('Unknown')
    });
}