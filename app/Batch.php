<?php namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public function getUser()
    {
        return User::find($this->id);
    }
}
