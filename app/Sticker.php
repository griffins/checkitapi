<?php namespace App;

use App\Batch;
use Illuminate\Database\Eloquent\Model;

class Sticker extends Model
{
    public function getBatch()
    {
        return Batch::find($this->id);
    }
}
