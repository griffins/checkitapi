<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;

class AuthController extends Controller
{


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Create a new authentication controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function destroy(Request $request)
    {
        JWTAuth::invalidate(JWTAuth::getToken());
    }

    public function postLogin(Request $request)
    {

        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                //return failed login message
                $view = view('error', ['message' => 'These credentials do not match our records.',
                    'code' => 401, 'type' => 'AuthException']);
                return response($view, 404, [])
                    ->header('Content-Type', 'application/json');
            }

        } catch (\Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500)->header('Content-Type', 'application/json');
        }


        $access = json_encode(compact('token'), JSON_PRETTY_PRINT);
        return response()->view('response', ['payload' => $access, 'code' => 200])->header('Content-Type', 'application/json');
    }

}
