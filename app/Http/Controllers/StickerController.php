<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Sticker;
use Input;
use JWTAuth;
use Validator;

class StickerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        return 'true';
    }

    public function enquire3()
    {
        $serial = Input::get('serial');
        if ($serial != null) {
            $u = Sticker::where('serial', $serial)->first();
            if ($u) {
                $u->batch = $u->getBatch();
                $u->batch->vendor = $u->batch->getUser();
                return response()->view('response', ['code' => 200, 'payload' => $u])->header('Content-Type', 'application/json');
            } else {
                return response()->view('error',
                    ['message' => "The resource requested couldnt be found",
                        'code' => 404, 'type' => 'NotFoundException']
                )->header('Content-Type', 'application/json');
            }
        } else {
            return response()->view('error',
                ['message' => 'Please provide a serial number.',
                    'code' => 401, 'type' => 'APIException']
            )->header('Content-Type', 'application/json');

        }
    }

    public function enquire()
    {
        $serial = Input::get('serial');
        if ($serial != null) {
            $u = Batch::where('serial', $serial)->first();
            if ($u) {
                $u->vendor = $u->getUser();
                if ($u->status = "SD") {
                    $u->status = "CR";
                    $u->save();
                    return response()->view('response', ['code' => 200, 'payload' => $u])->header('Content-Type', 'application/json');
                } else {

                }
            } else {
                return response()->view('error',
                    ['message' => "The resource requested couldnt be found",
                        'code' => 404, 'type' => 'NotFoundException']
                )->header('Content-Type', 'application/json');
            }
        } else {
            return response()->view('error',
                ['message' => 'Please provide a serial number.',
                    'code' => 401, 'type' => 'APIException']
            )->header('Content-Type', 'application/json');

        }
    }

    public function recieve()
    {
        $serial = Input::get('serial');
        if ($serial != null) {
            $u = Batch::where('serial', $serial)->first();
            if ($u) {
                $u->vendor = $u->getUser();
                return response(view('response', ['code' => 200, 'payload' => $u]), 200, [])->header('Content-Type', 'application/json');
            } else {
                $view = view('error', ['message' => "The batch requested couldnt be found", 'code' => 404, 'type' => 'NotFoundException']);
                return response($view, 404, [])->header('Content-Type', 'application/json');
            }
        } else {
            return response(view('error',
                ['message' => "Please supply serial",
                    'code' => 404, 'type' => 'APIException']
            ), 400, [])->header('Content-Type', 'application/json');
        }
    }
    public function dispatchB()
    {
        $serial = Input::get('serial');
        if ($serial != null) {
            $u = Batch::where('serial', $serial)->first();
            if ($u) {
                $u->vendor = $u->getUser();
                return response(view('response', ['code' => 200, 'payload' => $u]), 200, [])->header('Content-Type', 'application/json');
            } else {
                $view = view('error', ['message' => "The batch requested couldnt be found", 'code' => 404, 'type' => 'NotFoundException']);
                return response($view, 404, [])->header('Content-Type', 'application/json');
            }
        } else {
            return response(view('error',
                ['message' => "Please supply serial",
                    'code' => 404, 'type' => 'APIException']
            ), 400, [])->header('Content-Type', 'application/json');
        }
    }
    public function deactivate()
    {
        $serial = Input::get('serial');
        if ($serial != null) {
            $u = Batch::where('serial', $serial)->first();
            if ($u) {
                $u->vendor = $u->getUser();
                return response(view('response', ['code' => 200, 'payload' => $u]), 200, [])->header('Content-Type', 'application/json');
            } else {
                $view = view('error', ['message' => "The batch requested couldnt be found", 'code' => 404, 'type' => 'NotFoundException']);
                return response($view, 404, [])->header('Content-Type', 'application/json');
            }
        } else {
            return response(view('error',
                ['message' => "Please supply serial",
                    'code' => 404, 'type' => 'APIException']
            ), 400, [])->header('Content-Type', 'application/json');
        }
    }

    public function getUser()
    {

        $u = JWTAuth::parseToken()->authenticate();

        $u = json_encode($u, JSON_PRETTY_PRINT);
        $view = view('response', ['code' => 200, 'payload' => $u]);
        return response($view, 200, [])->header('Content-Type', 'application/json');
    }

}
