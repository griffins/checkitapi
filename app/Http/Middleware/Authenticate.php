<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Input;
use JWTAuth;

class Authenticate
{


    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!JWTAuth::parseToken()->authenticate()) {
                return response()->view('error', ['message' => 'You need to login first.', 'code' => 401, 'type' => 'AuthException'])->header('Content-Type', 'application/json');
            }
        } catch (\Exception $e) {
            return response(view('error', ['message' => $e->getMessage(), 'code' => 402, 'type' => 'AuthException']), 402, [])->header('Content-Type', 'application/json');
        }
        return $next($request);
    }

}
