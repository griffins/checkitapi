<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/test', 'StickerController@test');
Route::get('/user', 'StickerController@getUser');
Route::post('/auth', 'AuthController@postLogin');
Route::any('/recieve', 'StickerController@recieve');
Route::any('/dispatch', 'StickerController@dispatchB');
Route::any('/deactivate', 'StickerController@deactivate');
Route::any('/enquire', 'StickerController@enquire');
Route::delete('/auth', 'AuthController@destroy');
Route::any('/logout', 'AuthController@destroy');
Route::any('/logout.html', 'AuthController@destroy');
