<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/');

		$this->assertEquals(200, $response->getStatusCode());
	}
	/**
	 * A test to make sure we get json as content_type.
	 *
	 * @return void
	 */
	public function testContentType()
	{
		$response = $this->call('GET', '/enquire');

		$this->assertEquals('application/json', $response->header('content-type'));
	}

}
